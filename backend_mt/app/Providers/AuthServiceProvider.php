<?php

namespace App\Providers;

use Illuminate\Auth\GenericUser ;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

use Exception ;
use Firebase\JWT\TWT ;
use Firebase\JWT\ExpiredException ;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('api_token')) {

                $token = $request->input('api_token');
                
                try {
                    $credentials = JWT::decode($token, env('APP_KEY'),['HS256']);
                    return new GenericUser(['id' => $credentials->sub]);
                
                }catch (Exception $e) {
                    return null ;
                }
                
            }
        });
    }
}

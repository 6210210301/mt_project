<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Firebase\JWT\JWT ;  

// Login API (for both Admin & User)
$router->post('/login', function(Illuminate\Http\Request $request) {


	$username = $request->input("username");
	$password = $request->input("password");
	
	$result = app('db')->select("SELECT username , password  FROM login_mt WHERE username=?",
									[$username]);
									
	$loginResult = new stdClass();
	
	if(count ($result) == 0) {
			$loginResult->status = "fail";
			$loginResult->reason = "User is not founded";
	}else {
	
		if(app('hash')->check($password, $result[0]->password)){
			$loginResult->status = "success";
			
			$payload = [
				'iss' => "mt_threater",
				'sub' => $result[0]-> username,
				'iat' => time(),
				'exp' => time()+ 30 * 60 * 60,
			
			];
			
			$loginResult->token = JWT::encode($payload, env('APP_KEY'));
			$loginResult->isAdmin = $result[0]-> login_mt;
			
		}else {
			$loginResult->status = "fail";
			$loginResult->reason = "Incorrect Password";
		}
	}
	return response()->json($loginResult);
});
/*
//Public API (Can be called without API Token
$router->get('/register', function () {
	
	$results = app('db')->select("SELECT * FROM register");
	return response()->json($results);
});

$router->post('/register', function(Illuminate\Http\Request $request) {
	
	
	$Username = $request->input("username");
	$Surname = $request->input("surname");
	$Email = $request->input("email");
	$Password = app('hash')->make($request->input("password"));
	
	$query = app('db')->insert('INSERT into register
					(username, surname, email , password)
					VALUE (?, ?, ?, ?)',
					[
					  $Username,
					  $Surname,
					  $Email,
					  $Password,
					  'n'] );
	return "Ok";
	
});




    

